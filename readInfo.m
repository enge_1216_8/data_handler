function [auth, user, serverURL] = readInfo(filepath)
    file = fopen(filepath);
    str = fread(file, inf, '*char')';
    fclose(file);
    info = jsondecode(str);
    auth = info.authorization;
    user = info.user;
    serverURL = info.serverURL;
end
