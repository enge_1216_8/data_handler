[auth, user, serverURL] = readInfo("info.json");
fprintf("Using data from user %s\n\n", user);

previousId = -1;
first = false;

while true
    data = getData(auth, user, 100);

    if data(1).logId ~= previousId
        fprintf("Weight: %5.3f kg, Date: %s, Time: %s\n", ...
            data(1).weight, ...
            data(1).time, ...
            data(1).date);
        previousId = data(1).logId;
        if first
            first = false;
        else
            resp = sendData(serverURL, data(1));
        end
    end

    pause(0.5);
end
