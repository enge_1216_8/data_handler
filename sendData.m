function [resp] = sendData(serverURL, data)
    import matlab.net.http.*
    import matlab.net.http.field.*
    import matlab.net.*
    method = RequestMethod.POST;
    contentTypeField = matlab.net.http.field.ContentTypeField('application/json');
    uri = URI(sprintf("%s/api/update-weight", serverURL));
    body = matlab.net.http.MessageBody(jsonencode(data));
    header = [contentTypeField];
    request = RequestMessage(method, header, body);
    resp = send(request, uri);
    
end
